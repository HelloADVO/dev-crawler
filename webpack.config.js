module.exports = {
    // entry: [ 'whatwg-fetch', './src/scripts/treehouse.js' ],
    entry: {
        'codeschool': './src/scripts/codeschool.js',
        'egghead': './src/scripts/egghead.js',
        'laracasts': './src/scripts/laracasts.js',
        'treehouse': './src/scripts/treehouse.js'
    },
    output: {
        path: __dirname + '/build',
        publicPath: '/build/',
        filename: 'bundle-[name].js',
    }
}
