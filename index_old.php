<?php
    require 'functions.php';

    $topics = array(
        // 'android',
        'css',
        // 'csharp',
        // 'databases',
    );

    foreach ($topics as $key => $topic) {
        $url = 'https://teamtreehouse.com/library/topic:' . $topic;
        $results_page = curl($url);

        $results_page = scrape_between($results_page, '<main class="content " role="main" data-content="">', '<footer class="footer" role="contentinfo">');
        $results_page = preg_replace('/(<[^>]+) data-activity=".*?"/i', '$1', $results_page);
        $results_page = preg_replace('/(<[^>]+) data-location=".*?"/i', '$1', $results_page);
        $results_page = preg_replace('/(<[^>]+) data-featurette=".*?"/i', '$1', $results_page);
        $results_page = preg_replace('/(<[^>]+) id=".*?"/i', '$1', $results_page);
        $results_page = preg_replace('/(<[^>]+) add-topic-background-color/i', '$1', $results_page);
        // $results_page = preg_replace('card course syllabus topic-', 'card topic-', $results_page);
        // $results_page = preg_replace('card workshop topic-', 'card topic-', $results_page);

        $separate_results = explode('<li class="card course syllabus topic-' . $topic . '">', $results_page);

        // For each separate result, scrape the URL
        foreach ($separate_results as $key => $separate_result) {
            if ($separate_result != '' && $pos = strpos($separate_result, '<a class="card-box"') !== false) {
                $course = [];

                $course['type'] = scrape_between($separate_result, '<strong class="card-type">', '</strong>');
                $course['url'] = scrape_between($separate_result, '<a class="card-box" href="', '">');
                $course['title'] = scrape_between($separate_result, '<h3 class="card-title">', '</h3>');
                $course['description'] = scrape_between($separate_result, '<p class="card-description">', '</p>');
                $course['difficulty'] = scrape_between($separate_result, '<li class="difficulty"><span>', '</span></li>');
                $course['duration'] = scrape_between($separate_result, '<span class="card-estimate">', '</span>');
                $course['categories'] = $topic;

                $courses[] = $course;
            }
        }
    }

    echo '<pre>';
        print_r($courses);
    echo '</pre>';
?>
