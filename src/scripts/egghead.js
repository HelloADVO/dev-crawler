'use strict';

const DOMPurify = require('dompurify');
const axios = require('axios');
const siteUrl = 'https://egghead.io';
const libraryUrl = siteUrl + '/technologies/';
const topics = [ 'angularjs', 'angular2', 'css', 'd3', 'elm', 'html', 'js', 'node', 'react', 'rx', 'typescript' ];

let promises = [];
let courses = [];

const sanitize = function (dirty) {
    return DOMPurify.sanitize(dirty);
};

const doYourJob = function (html) {
    const el = document.createElement('div');
    el.innerHTML = html;

    getCourses(el);
};

const getCourses = function (el) {
    const cards = el.querySelectorAll('.subsection-courses .card-course');

    Array.prototype.forEach.call(cards, function (card) {
        getCourseData(card);
    });
};

const getCourseData = function (card) {
    const tags = card.querySelector('.card-tags');
    const course = {
        // id: parseInt(card.getAttribute('data-activity').split('/')[1], 10),
        url: siteUrl + sanitize( card.querySelector('.link-overlay').getAttribute('href') ),
        // type: sanitize( card.querySelector('.card-type').textContent ),
        title: sanitize( card.querySelector('.course-title').textContent ),
        description: sanitize( card.querySelector('.description-text').textContent ),
        // duration: sanitize( card.querySelector('.card-estimate').textContent ),
        // topic: tags.querySelector('.topic') !== null ? sanitize( tags.querySelector('.topic span').textContent ) : 'undefined',
        // difficulty: tags.querySelector('.difficulty') !== null ? sanitize( tags.querySelector('.difficulty span').textContent ) : 'undefined'
    };

    courses.push(course);
};

// We have to use 'let' instead of 'const' because of Firefox... Great.
for (let topic of topics) {
    promises.push( axios(libraryUrl + topic) );
}

axios
    .all(promises)
    .then(responses => {
        responses.map(response => doYourJob(response.data));
        console.log(courses);
    })
    .catch(err => {
        console.log( Error(err) );
    });
