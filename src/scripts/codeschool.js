'use strict';

const DOMPurify = require('dompurify');
const axios = require('axios');
const siteUrl = 'https://www.codeschool.com';
const libraryUrl = siteUrl + '/learn/';
const topics = [ 'html-css' ];

let promises = [];
let courses = [];

const myHeaders = new Headers();
const myInit = {
    // method: 'GET',
    // headers: myHeaders,
    mode: 'no-cors',
    // cache: 'default'
};

const sanitize = function (dirty) {
    return DOMPurify.sanitize(dirty);
};

const doYourJob = function (html) {
    const el = document.createElement('div');
    el.innerHTML = html;

    getCourses(el);
};

const getCourses = function (el) {
    const cards = el.querySelectorAll('.card');

    Array.prototype.forEach.call(cards, function (card) {
        getCourseData(card);
    });
};

const getCourseData = function (card) {
    console.log(card);
    const course = {
        // id: parseInt(card.getAttribute('data-activity').split('/')[1], 10),
        url: siteUrl + sanitize( card.querySelector('.course-title-link').getAttribute('href') ),
        // type: sanitize( card.querySelector('.card-type').textContent ),
        title: sanitize( card.querySelector('.course-title-link').textContent ),
        description: sanitize( card.querySelector('.course-tagline').textContent ),
        // duration: sanitize( card.querySelector('.card-estimate').textContent ),
        // topic: tags.querySelector('.topic') !== null ? sanitize( tags.querySelector('.topic span').textContent ) : 'undefined',
        // difficulty: tags.querySelector('.difficulty') !== null ? sanitize( tags.querySelector('.difficulty span').textContent ) : 'undefined'
    };

    courses.push(course);
};

// We have to use 'let' instead of 'const' because of Firefox... Great.
for (let topic of topics) {
    const myRequest = new Request(libraryUrl + topic, myInit);
    // promises.push( fetch(myRequest, myInit) );
    promises.push( axios(libraryUrl + topic) );
}

axios
    .all(promises)
    .then(responses => {
        return Promise.all(responses.map(response => {
            console.log(response);
            // response.text();
        }));
    })
    .then(responses => {
        responses.map(response => doYourJob(response.data));
        console.log(courses);
    })
    .catch(err => {
        console.log( Error(err) );
    });
